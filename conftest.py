from  pytest import fixture
from util.conn import Connection
from util.config import *
import requests

@fixture(scope="session",params=[(username,password,GRANT_TYPE)])
def test_setup():
    print("test_setup called")
    login_url = eos_url + "oauth/token"

    payload = 'username=' + username + '&password=' + password + '&grant_type=' + GRANT_TYPE
    response = Connection.get_session().post(login_url, headers=Connection.get_login_headers(), data=payload)
    print(login_url)
    print(response.text)

    Connection.set_token(response)
