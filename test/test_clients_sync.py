import json
from util.conn import Connection
from util.config import *
from util.lib import from_dob_to_age,validate_myanmar_nrc,validate_myanmar_phnum

from pytest import mark,fixture
from test_data import client_schema
#from schema import Schema, And, Use, Optional

@fixture(scope = "module")
def test_client_sync(test_setup):
    try:
        session = Connection.get_session()
        response = session.get(clientsync_url, headers=Connection.get_headers(), data={})
        return response
    except Exception as ex:
        print(ex)
        assert False

@mark.BVT
def test_client_sync_schema(test_client_sync):
    """
    This test is validating clients sync api schema. Validating the status code
    #TODO : Need to explore in detail
    """
    try:
        response = test_client_sync
        assert response.status_code == 200
        #TODO : Response message "Clients synced successfully"
        json_obj = json.loads(response.text.encode("utf-8"))
        print(str(json_obj).encode('utf8'))
        validated = client_schema.test_client_schema.validate(json_obj)
        #print("validated= " + str(validated).encode('utf8'))
        print(client_schema.test_client_schema.is_valid(json_obj))

    except Exception as ex:
        print(ex)
        assert False

@mark.BVT
def test_client_id(test_client_sync):
    """
        API : v1/clients/clientid
        This test validate all the clients mandatory fields should not  be null
        Age of the client should be equal to greater than 18 if the field is not null
        TODO : URL need change to sdui
    """
    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):

            if d.get("id"):
                client_url= eos_url +"v1/clients/"+str(d.get("id"))
                print(client_url)
                session = Connection.get_session()
                response = session.get(client_url, headers=Connection.get_headers(), data={})
                assert response.status_code == 200
                #print(response.text.encode())
                json_obj=json.loads(response.text)
                if not json_obj["data"]["accountNo"]:
                    assert False
                if not json_obj["data"]["clientStatus"]:
                    assert False
                #if not json_obj["data"]["activationDate"]: #If the client is not approved just submitted
                #    assert False
                if not json_obj["data"]["firstname"]:
                    assert False
                if not json_obj["data"]["lastname"]:
                    assert False
                if not json_obj["data"]["displayName"]:
                    assert False
                if not json_obj["data"]["officeId"]:
                    assert False
                if not json_obj["data"]["officeName"]:
                    assert False
                if json_obj["data"]["dateOfBirth"]:
                    age=json_obj["data"]["dateOfBirth"]
                    current_age=from_dob_to_age(age)
                    print(current_age)
                    assert current_age >= 18

    except Exception as ex:
        print(ex)
        assert False

@mark.BVT
def test_client_image(test_client_sync):
    """
    This test is validating the images of the clients, if field "imagePresent" in client sync api returns true
    then validate the response and image in response text.
    """

    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            if d.get("imagePresent") :
                image_url = client_images + str(d.get("id")) + "?maxHeight=150"
                print(image_url)
                session = Connection.get_session()
                response = session.get(image_url, headers=Connection.get_headers(), data={})
                print(response.text)
                assert response.status_code == 200
                assert "image" in response.text
                #TODO need to check the size of images

    except Exception as ex:
       print(ex)
       assert False

@mark.BVT
def test_client_document(test_client_sync):
    """
    This test is validating client documents in the form of JPEG\png and response status code.
    """

    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            documents = d.get("documents")
            #TODO : Need to check if document can be empty of not
            if documents is not None:
                for doc_id in documents:
                    doc_url = client_images+ str(d.get("id"))+"/documents/"+str(doc_id.get("id"))
                    print(doc_url)
                    session = Connection.get_session()
                    response = session.get(doc_url, headers=Connection.get_headers(), data={})
                    assert response.status_code == 200
                    assert str(doc_id.get("type") in ("image/png","image/jpeg"))
                    #TODO need to check the number of images.
    except Exception as ex:
        print(ex)
        assert False

@mark.BVT
def test_client_loan(test_client_sync):
    """
    This test is validating all the number of loans for the client.
    Loan API status,Total totalRepaymentExpected = totalPrincipalDisbursed+totalInterestCharged+fees
    totalOutstanding = totalRepaymentExpected - totalPaidInAdvance
    Num of periods = numberOfRepayments+1
    Todo : need to verify Arrears functionality
    """
    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            loan = d.get("loans")

            if loan is not None :
                for loan_id in loan:
                    loan_url = eos_url +"v1/loans/"+str(loan_id.get("id"))

                    session = Connection.get_session()
                    response = session.get(loan_url, headers=Connection.get_headers(), data={})
                    #print(response.text.encode("utf-8"))
                    assert response.status_code == 200
                    principal = loan_id["repaymentSchedule"]["totalPrincipalDisbursed"]
                    interst = loan_id["repaymentSchedule"]["totalInterestCharged"]
                    fees =  loan_id["repaymentSchedule"]["totalFeeChargesCharged"]
                    total_amount = principal+fees+interst
                    assert loan_id["repaymentSchedule"]["totalRepaymentExpected"] == total_amount
                    payment= loan_id["repaymentSchedule"]["totalRepaymentExpected"]
                    adv_pay = loan_id["repaymentSchedule"]["totalPaidInAdvance"]
                    out_amt = loan_id["repaymentSchedule"]["totalOutstanding"]
                    assert out_amt == payment - adv_pay
                    #print(loan_id.get("inArrears"))
                    num_of_periods = loan_id.get("numberOfRepayments")+1
                    #print(num_of_periods)
                    assert num_of_periods == len(loan_id["repaymentSchedule"]["periods"])
                    if loan_id.get("inArrears") == False:
                        pass
                    else:
                        pass

                    #TODO need to check in case of Arrears is true
                    #TODO need to check on number of loans
                    #TODO Check for the loan amount gretaer than 5 Million than need CM approval.

    except Exception as ex:
        print(ex)
        assert False

@mark.BVT
def test_client_Summary(test_client_sync):
    """
    Validate clientSummary[loanAmount] should be equal to totalRepaymentExpected of the latest loan.Compare Period 0 due date with the latest one
    TODO : Need to picked the recent loan
    """
    response = test_client_sync

    try:
        json_obj = json.loads(response.text.encode("utf-8"))

        for d in json_obj.get('data'):

            if  d["clientSummary"]["loanAmount"] != 0.0:
                print(d["clientSummary"]["loanAmount"])
                loan = d.get("loans")
                for loan_id in loan:
                    #TODO : Need to compare the dates and picked the latest due date
                    loan_date = str(loan_id["repaymentSchedule"]["periods"][0]["dueDate"])
                    print(loan_date)

    except Exception as ex:
        print(ex)
        assert False

@mark.BVT
def test_validate_clientDetails(test_client_sync):
    """
        This test will validate the myanmar phone number and national id field if both the fields are present in client details
    """
    response = test_client_sync
    try:
        json_obj = json.loads(response.text.encode("utf-8"))
        for d in json_obj.get('data'):
            client_detail = d.get("clientDetails")
            if client_detail:
                ph_num=client_detail.get("phoneNumber1")
                national_id = client_detail.get("nationalIdNumber")
                if national_id:
                    assert validate_myanmar_nrc(national_id)
                if ph_num:
                    assert  validate_myanmar_phnum(ph_num)

    except Exception as ex :
        print(ex)
        assert False

