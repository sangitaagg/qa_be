import datetime
from myanmar import phonenumber
from myanmar import nrc

def from_dob_to_age(born):
    born = datetime.datetime.strptime(born, '%Y-%m-%d')
    today = datetime.date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

def validate_myanmar_phnum(ph_num):
    return (phonenumber.is_valid_phonenumber(ph_num))

def validate_myanmar_nrc(nrc_num):
    return (nrc.is_valid_nrc(nrc_num))

#validate_myanmar_phnum('09876543210')
#validate_myanmar_phnum('09971743246')
#validate_myanmar_phnum('09260178851')
#validate_myanmar_phnum('09453483883')
#validate_myanmar_nrc("13/LaLaNa(N)029231")
#validate_myanmar_nrc("2/PhaYaSa(N)005080")
#validate_myanmar_nrc("2/LaKaNa(N)043041")
#validate_myanmar_nrc("9/TaThaNa(N)096802")
